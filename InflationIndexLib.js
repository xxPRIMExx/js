const cheerio = require('cheerio');     
const request = require('request');
const cheerioTableparser = require('cheerio-tableparser');

let date = new Date();
let table = null;

module.exports.getInflationOnDate = function (month, year, cb){
    request('https://index.minfin.com.ua/ua/economy/index/inflation/', function(err, responce, body){
    if(!err && responce.statusCode == 200){
        //console.log('download complete');
        let $ = cheerio.load(body);
        cheerioTableparser($);
        table = $("table").parsetable();
        //
        //console.log(getValueFromTable(year, month, table));
        //
        return cb(getValueFromTable(year, month, table));
    }else{
        return err;
    }
    });
}

// Получеие значения инфляции
function getValueFromTable(year, month, table){
    if(year < 2000 || year > date.getFullYear() || month < 1 || month > 12){
        return 0;
    }

    return table[month][getIndexFromYear(year)];
}

// Получение индекса строки таблицы
function getIndexFromYear(year){
    return year - 2000 + 1;
}