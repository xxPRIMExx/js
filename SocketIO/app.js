// 1. Простой сервер
// var app = require('express')();
// var http = require('http').Server(app);
 
// app.get('/', function(req, res){
//     res.sendFile(__dirname + '/index.html');
// });
 
// http.listen(3000, function(){
//     console.log('Server al loclahost:3000');
// });
 
// 2.Socket
// var app = require('express')();
// var http = require('http').Server(app);
// var io = require('socket.io')(http);
 
// app.get('/', function(req, res){
//     res.sendFile(__dirname + '/index.html');
// });
 
// io.on('connection', function(socket){
//     console.log('A user connected');
 
//     socket.on('disconnect', function(){
//         console.log('A user disconnected');
//     });
// });
 
// http.listen(3000, function(){
//     console.log('Server at lockalhost:3000');
// });

// 3. Обработка событий
// const app = require('express')();
// const http = require('http').Server(app);
// const io = require('socket.io')(http);

// app.get('/', function(req, res){
//     res.sendFile(__dirname + '/index.html');
// });

// io.on('connection', function(socket){
//     console.log('a user connected');

//     setTimeout(function(){
//         socket.send("Send message 2 seconds after connection");
//     }, 2000);

//     socket.on('disconnect', function(){
//         console.log("a user disconnected");
//     })
// });

// http.listen(3000, function(){
//     console.log('Server at lockalhost:3000');
// });

// 4. Добавление пользовательских событий
// const app = require('express')();
// const http = require('http').Server(app);
// const io = require('socket.io')(http);

// app.get('/', function(req, res){
//     res.sendFile(__dirname + '/index.html');
// });

// io.on('connection', function(socket){
//     console.log('A user connected');

//     setTimeout(function(){
//         // Отправка своего обытия и объекта клиенту
//         socket.emit('myEvent', { description: 'User event from server',
//                                  userName: 'UserName' });
//     }, 2000);

//     socket.on('disconnect', function(){
//         console.log('A user disconnected');
//     });
// });

// http.listen(3000, function(){
//     console.log('Server at lockalhost:3000');
// });

// 5. Получение событий от клиента
// const app = require('express')();
// const http = require('http').Server(app);
// const io = require('socket.io')(http);

// app.get('/', function(req, res){
//     res.sendFile(__dirname + '/index.html');
// });

// io.on('connection', function(socket){
//     socket.on('clientEvent', function(data){
//         console.log(data);
//     });
// });

// http.listen(3000, function(){
//     console.log('Server at lockalhost:3000');
// });

// 6. Широковещание
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
let users = 0;

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
    users++;
    socket.broadcast.emit('broadcast', {description: users + ' users connected'});
    socket.on('disconnect', function(){
        users--;
        socket.broadcast.emit('broadcast', {description: users + ' users connected'});
    });
});

http.listen(3000, function(){
    console.log('Server at lockalhost:3000');
});