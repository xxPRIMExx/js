const express = require('express');
const app = express();

app.use(function(req, res, next){
    console.log("Middleware-1");
    next();
});

app.use(function(req, res, next){
    console.log("Middleware-2");
    //next();
});

app.get("/", function(req, res){
    console.log("Route /");
    res.send("Hello");
});

app.listen(3000);