const EventEmmitter = require("events");
const EEmmitter = new EventEmmitter();

EEmmitter.on("login", function (name) {
    if(name == "user"){
        EEmmitter.emit("loginSucceded");
    }else{
        EEmmitter.emit("loginFault");
    }
})

EEmmitter.on("loginSucceded", function(){
    console.log("Вход выполнен");
    EEmmitter.emit("redirectToAccount");
});

EEmmitter.on("loginFault", function(){
    console.log("Вход не выполнен");
    EEmmitter.emit("redirectToLogin");
});

EEmmitter.on("redirectToLogin", function(){
    console.log("Возврат на страницу ввода...")
});

EEmmitter.on("redirectToAccount", function(){
    console.log("Переход в личный кабинет...")
});



EEmmitter.emit("login", "user");
EEmmitter.emit("login", "user1");