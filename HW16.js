const cheerio = require('cheerio');
const request = require('request');
const cheerioTableparser = require('cheerio-tableparser');

let date = new Date();
let table = null;

getInflationOnDate(2000, 0);

function getInflationOnDate(year, month){
    request('https://index.minfin.com.ua/ua/economy/index/inflation/', function(err, responce, body){
    if(!err && responce.statusCode == 200){
        console.log('download complete');
        let $ = cheerio.load(body);
        cheerioTableparser($);
        table = $("table").parsetable();
        console.log(getValueFromTable(year, month, table));

    }else{
        console.log(err)
    }
    });
}

function getValueFromTable(year, month, table){
    if(year < 2000 || year > date.getFullYear() || month < 1 || month > 12){
        return 0;
    }

    return table[getIndexFromYear(year)][month];
}

function getIndexFromYear(year){
    return year - 2000 + 1;
}