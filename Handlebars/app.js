const express = require("express");
const expressHbs = require('express-handlebars');
const app = express();
const hbs = require('hbs');
const port = 3000;

hbs.registerPartials(__dirname + "/views/partials");
app.set("view engine", "hbs");

app.get('/', function(req, res){
    res.render('home.hbs');
});

app.engine('hbs', expressHbs({
    layoutsDir: 'views/layouts',
    defaultLayout: 'layout',
    extname: 'hbs'
}));

app.use('/contact', function(req, res){
    res.render('contact.hbs',{
        title: "Мои контакты",
        emails: ["gavgav@mycorp.com", "mioaw@mycorp.com"],
        emailVisible: true,
        phone: "+380933332211"
    });
});

app.listen(port, ()=>{
    console.log('localhost:3000 server is started');
});