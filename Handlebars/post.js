const express = require('express');
const app = express();
const port = 3000;

app.get('/', function(req, res){
    res.send('Hello from Express [GET]');
});

app.post('/', function(req, res){
    res.send('Hello from Express [POST]');
});

app.get('/example/a', function(req, res){
    res.send('Hello rom A');
});

app.get('/example/b', function(req, res, next){
    console.log('this responce will be send by the next function...');
    next();
}, function(req, res){
    res.send('Hello from NEXT_FUNCTION from B');
});

let cb0 = function (req, res, next) {
    console.log('CB0');
    next();
  }
  
let cb1 = function (req, res, next) {
    console.log('CB1');
    next();
  }
  
let cb2 = function (req, res) {
    res.send('Hello from C!');
  }

app.get('/example/c', [cb0, cb1, cb2]);

app.route('/book/')
  .get(function(req, res) {
    res.send('Get a random book');
  })
  .post(function(req, res) {
    res.send('Add a book');
  })
  .put(function(req, res) {
    res.send('Update the book');
  });

app.listen(port, ()=>{
    console.log('localhost:3000 server is started');
});