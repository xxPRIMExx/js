// Подключение библиотеки
const fs = require('fs');
// Данные для записи
let data = "test data from script";
// Запись/Создание файла с данными
fs.writeFileSync("writefile.txt", data, "utf-8");
// Добавление данных в существующий файл. Если файл не создан. Он будет создан
let data2 = "\nnew Data For append";
fs.appendFileSync("writefile.txt", data2, "utf-8");
//----------------------------------------------------------------------------

const obj = {
    b: 2
};

const obj2 ={
    ...obj,
    c: 2
}

console.log(obj2.b)