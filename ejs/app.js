const express = require("express");
const app = express();
const port = 3000;

app.set("view engine", "ejs");

app.get('/', function(req, res){
    res.send("Main page!!");
});

app.use('/contact', function(req, res){
    res.render('contact',{
        title: "Мои контакты",
        emails: ["gavgav@mycorp.com", "mioaw@mycorp.com"],
        emailVisible: true,
        phone: "+380933332211"
    });
});

app.listen(port, ()=>{
    console.log('localhost:3000 server is started');
});