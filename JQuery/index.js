// $(function () 
// {  
//     $('p').on('click', function(){
//         let currentColor = $(this).css('color');
//         $('body').css('background', currentColor);
//     });

//     $('p:not("[class]")').attr('class', 'red');
// });

$(document).ready(function(){
    let btn = $('button:first');
    let color;

    btn.on('click', function(){
        console.log('click');
        $('#modal').addClass('show');
    });

    $('#modal>#content span').on('click', function(){
        $('#modal').removeClass('show');
    });

    // $('li').on('click', function(){
    //     console.log($(this).text());
    //     color = $(this).text();
    //     $('#content').css('background', color);
    // });

    // $('button#submit').on('click', function(){
    //     $("#modal").css('background', color);
    //     $('#content').css('background', 'white');
    // });
    // Делегирование на дочерний элемент
    $('ul').on('click', 'li', function(){
        const el = $(this);
        reset();        
        const color = el.text();
        
        el.addClass('active');
        $('#submit').removeAttr('disabled');
    });

    function reset(){
        $('li').removeClass('active');
    }

    $("#ok").on('click', function(){
        $('#modal').removeClass('show')
     });

 });
