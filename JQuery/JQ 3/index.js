class User{
    constructor(id, firstName, lastName, age, role,){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.role = role;
    }
}

$(document).ready(function(){
    const firstName = $('#firstName');
    const lastName = $('#lastName');
    const age = $('#age');
    const role = $('#role');

    let userArray = [
        new User(1, 'Ivan', 'Ivanov', 21,  'admin'),
        new User(2, 'Petr', 'Petrov', 42, 'employee'),
        new User(3, 'Alex', 'Stepanov', 30, 'client')
    ];

    function getMaxValue(array){
        return Math.max.apply(Math, userArray.map(function(usr) { return usr.id; }));
    }  

    function fillForm(el){
        firstName.val(el.firstName);
        lastName.val(el.lastName);
        age.val(el.age);
    }

    function showAll(){
        tableDestination.empty();
        $('div.alert.alert-danger').remove();

        userArray.forEach(function(usr, index){
            tableDestination.append('<tr>' 
            + '<td>' + usr.id + '</td>' 
            + '<td>' + usr.firstName + '</td>'  
            + '<td>' + usr.lastName + '</td>' 
            + '<td>' + usr.age + '</td>'
            + '<td><button class="btn btn-danger" data-id="' + usr.id + '\">' + 'Удалить' + '</button>'
            + '<button class="btn btn-info" data-id="' + usr.id + '\">' + 'Редактировать' + '</button></td>'
            + '</tr>');

            tableDestination.find('button[data-id="'+ usr.id + '"]:contains("Удалить")').on('click', function(){
                let that = this;
                userArray = userArray.filter(function(user, i){
                    console.log(user.id);
                    return user.id != $(that).data().id;                    
                });
                showAll();
            });

            tableDestination.find('button[data-id="'+ usr.id + '"]:contains("Редактировать")').on('click', function(){
                let that = this;
                const elem = userArray.find(function(user, i){
                    if(user.id == $(that).data().id){
                        return user;
                    };                      
                });
                
                console.log(elem);
                fillForm(elem);
            });       
        });
    };



    console.log(userArray);
    let tableDestination = $('.table tbody');
    console.log(tableDestination);

    $('#btn').on('click', function(){
        tableDestination.empty();
        showAll();       
    });

    $('form').submit(function(e){
        e.preventDefault();
        let isFault = false;

 
        $('div.alert.alert-danger').remove();
        if(!firstName.val()){
            console.log('Name is empty');
            firstName.after('<div class="alert alert-danger">Required</div>');
            isFault = true;
        }
        if(!lastName.val()){
            console.log('last name is empty');
            lastName.after('<div class="alert alert-danger">Required</div>');
            isFault = true;
        }
        if(!age.val()){
            console.log('age is empty');
            age.after('<div class="alert alert-danger">Required</div>');
            isFault = true;
        }
        if(!role.val()){
            console.log('role is empty');
            isFault = true;
        }

        if(isFault)
            return;
        
        let maxId = getMaxValue(userArray) + 1;
        userArray.push(
            new User(
                    maxId,
                    $(this).find('[name="firstName"]').val(),
                    $(this).find('[name="lastName"]').val(),
                    $(this).find('[name="age"]').val(),
                    $(this).find('[name="role"]').val(),    
                    ));

        firstName.val('');
        lastName.val('');
        age.val('');
        
        showAll();

    });

  

});


