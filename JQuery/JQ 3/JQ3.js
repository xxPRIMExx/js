class Message{
    constructor(id, message){
        this.id = id;
        this.message = message;
    }
}

$(function(){
    const messageFld = $('#newMessage');
    let destination = $('#destination');
    let btn = $('getAll');

    let array = [
        new Message(1, 'message from js')
    ];

    array.forEach(function(item, i){
        console.log(item);
    });

    $('form').submit(function(e){
        e.preventDefault();
 
        let maxId = getMaxValue(array) + 1;
        
        appendMessage($(this).find('[name="msg"]').val());
        
        array.forEach(function(item, i){
            console.log(item);
        });
    
    });

    function getMaxValue(array){
        return Math.max.apply(Math, array.map(function(msg) { return msg.id; }));
    }  

    function appendMessage(message){
        $('#getAll').before('<div class="alert alert-success"> <span>' + message + '</span> <input type="checkbox" class="chbox"> </div>');

        $("input[type='checkbox']").change(function(){
            if(this.checked){
                $(this).prev().wrap('<del></del>')
            }
            else{
                $(this).parent().find('span').unwrap();
            }
            
       
        });
    };
});