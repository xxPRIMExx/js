$(function(){
	 let item = JSON.parse(localStorage.getItem('item'));
	// console.log(item.name + " " + item.password + " " + item.email);
	
	if(!item)
		location.replace("./register.html");

	let btn = document.getElementById("button");

	btn.addEventListener("click", login);

	function login(){	
		let email = document.getElementById("email");
		let pass = document.getElementById("pass");
		
		let isEmailCorrect = email.value === item.email;
		let isPassCorrect = pass.value === item.password;
		
		console.log("Pass " + pass.value + " " + item.password);
		console.log(isEmailCorrect, isPassCorrect);
		
		if(!isEmailCorrect)
			email.style.background = "DarkSalmon";
		
		if(!isPassCorrect)
			pass.style.background = "DarkSalmon";
		
		if(isEmailCorrect && isPassCorrect){
			location.href = "~/profile.html";	
			localStorage.setItem("isAuth", true);
		}
	}

});

