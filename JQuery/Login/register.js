$(function(){
	console.clear();
	let btn = $("#button");
	btn.on('click', Register);

	function Register(){
		$('p').removeClass('warning').addClass('.msg');

		let name = $("#name");
		let phone = $("#phone");
		let email = $("#email");
		let pass = $("#pass");
		let confirmPassword = $("#confirm-pass");
		console.log(name);
		
		let nameCorrect = isNameCorrect(name.val());
		let phoneCorrect = isPhoneCorrect(phone.val());
		let emailCorrect = isMailCorrect(email.val());
		let passCorrect = isPasswordCorrect(pass.val());
		let confirmPass = isPassConfirmOk(pass.val(), confirmPassword.val())
		
		if(!nameCorrect){
			setBackground(name, "DarkSalmon");
			$("#name ~ span").removeClass('msg').addClass("warning");
		}
		
		if(!phoneCorrect)
			setBackground(phone, "DarkSalmon");
		
		if(!emailCorrect)
			setBackground(email, "DarkSalmon");
		
		if(!passCorrect)
			setBackground(pass, "DarkSalmon");
		
		if(!confirmPass)
			setBackground(confirmPass, "DarkSalmon");
		
		if(nameCorrect && phoneCorrect && emailCorrect && passCorrect && confirmPass){
			let user = {
				name: name,
				password: pass,
				email: email
			}
			localStorage.setItem('item', JSON.stringify(user));
			location.href = "file:///C:/Users/student/Desktop/PracticeJS/login.html";
		}
		
	}

	function setBackground(el, color){
		el.css('background-color', color);
	}

	function isNameCorrect(name){
		let pattern = RegExp(/\w{3,}/);
		return pattern.test(name);
	}

	function isMailCorrect(mail){
		let pattern = RegExp(/\w{4,10}@\w{4,10}\.\w{2,}/);
		return pattern.test(mail);
	}

	function isPhoneCorrect(phone){
		let pattern = RegExp(/^\+380\d{9}$|\(\d{3}\)\d{3}-\d{2}-\d{2}$/);  
	return pattern.test(phone);
	}

	function isPasswordCorrect(password){
		let pattern = RegExp(/\w{4,}/)
	return pattern.test(password);
	}

	function isPassConfirmOk(pass, passCofirm){
		return pass === passCofirm;
	}
});
