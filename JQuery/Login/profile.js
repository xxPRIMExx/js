let isAuth = localStorage.getItem("isAuth");
if(!isAuth){
	location.href = "file:///C:/Users/student/Desktop/PracticeJS/login.html";
}

let removeElements = document.getElementsByClassName("remove");
for(let i = 0; i < removeElements.length; i++){
	removeElements[i].addEventListener('click', removeCurrent);
}


let item = JSON.parse(localStorage.getItem('item'));
let nameItem = document.getElementsByClassName("name")[0];
nameItem.textContent += " " + item.name;

let btnMessage = document.getElementById("button");
btnMessage.addEventListener('click', addComment);

let btnLogout = document.getElementById("logout");
btnLogout.addEventListener("click", logout);

let btnRemove = document.getElementById("removeBtn");
btnRemove.addEventListener('click', removeComment);

function addComment(){
	
	let message = document.getElementById("record");	
	let parent = document.getElementsByClassName("comment-block")[0];	
	
	let newRemove = document.createElement("span");
	newRemove.className = "remove";
	newRemove.textContent = "Удалить";
	newRemove.addEventListener('click', removeCurrent);
	
	let newComment = document.createElement("div");
	newComment.className = "comment";
	newComment.textContent = message.value;
	newComment.appendChild(newRemove);

	parent.appendChild(newComment);
	message.value = '';
}

function logout(){
	localStorage.setItem("isAuth", false);
	location.href = "file:///C:/Users/student/Desktop/PracticeJS/login.html";		
}

function removeComment(){
	let elemenents = document.getElementsByClassName("comment");
	if(elemenents.length > 0)
		elemenents[elemenents.length-1].remove();
}

function removeCurrent(e){
	let target = e.target.parentElement;
	target.remove();
}
