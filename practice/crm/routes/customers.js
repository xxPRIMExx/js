module.exports.all = function(req, res){
    req.getConnection(function(err, connection){
        connection.query("Select * from customers", function(err, rows){
            if(err) throw new Error;

            res.render('customers', {page_title: 'Пользователи', data: rows});
        });
    });
};

module.exports.add = function(req, res){
    res.render('add_customer', {page_title: 'добавить пользователя в систему'});
};

module.exports.edit = function(req, res){
    let id = req.params.id;
    req.getConnection(function(err, connection){
        connection.query("SELECT * from customers where id = ?", [id], function(err, rows){
            if(err) throw new Error;
            res.render('edit_customer', {page_title: "Изменить пользователя в системе", data: rows});
        });
    });
};

module.exports.save = function(req, res){
    let input = req.body;
    console.log(req.body);
    req.getConnection(function(err, connection){
        let data = {
            name: input.name,
            address: input.address,
            email: input.email,
            phone: input.phone
        };
        connection.query("INSERT INTO customers SET ?", [data], function(err, rows){
            if(err) throw new Error;
            res.redirect('/customers');
        });
    });
};

module.exports.edit_save = function(req, res){
    let input = req.body;
    let id = req.params.id;
    req.getConnection(function(err, connection){
        let data = {
            name: input.name,
            address: input.address,
            email: input.email,
            phone: input.phone
        };
        connection.query('UPDATE customers SET ? where id = ?', [data, id], function(err, row){
            if(err) throw Error;
            res.redirect('/customers');
        });
    });
};

module.exports.delete = function(req, res){
    let id = req.params.id;
    req.getConnection(function(err, connection){
        connection.query('DELETE FROM customers WHERE id = ?', [id], function(err, rows){
            if(err) throw Error;
            res.redirect('/customers');
        });
    });
};


